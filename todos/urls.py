from django.urls import path
from todos.views import (
    todo_list,
    todo_items,
    todo_list_detail,
    todo_list_create,
    todo_list_update,
    your_signup_view,
    your_login_view,
    your_logout_view,
)


app_name = "todos"

urlpatterns = [
    path("", todo_list, name="todo_list_list"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("create/", todo_list_create, name="todo_list_create"),
    path("<int:todo_list_id>/items/", todo_items, name="todo_items"),
    path("<int:id>/edit/", todo_list_update, name="todo_list_update"),
    path('signup/', your_signup_view, name='signup'),
    path('login/', your_login_view, name='login'),
    path('logout/', your_logout_view, name='logout'),
]
