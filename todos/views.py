from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoListForm
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout

@login_required
def todo_list(request):
    todo_lists = TodoList.objects.all()
    context = {"todo_lists": todo_lists}
    return render(request, "todos/list.html", context)

@login_required
def todo_items(request, todo_list_id):
    todo_list = get_object_or_404(TodoList, id=todo_list_id)
    todo_items = todo_list.items.all()
    return render(
        request,
        "todos/todo_items.html",
        {"todo_list": todo_list, "todo_items": todo_items},
    )

@login_required
def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    todo_items = todo_list.items.all()
    context = {"todo_list": todo_list, "todo_items": todo_items}
    return render(request, "todos/detail.html", context)

@login_required
def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm()
    return render(request, "todos/create.html", {"form": form})

@login_required
def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm(instance=todo_list)
    return render(
        request, "todos/update.html", {"form": form, "todo_list": todo_list}
    )

def your_signup_view(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)  # Log the user in after signup
            return redirect('todos:todo_list_list')  # Redirect to a page after signup
    else:
        form = UserCreationForm()
    return render(request, 'todos/signup.html', {'form': form})

def your_login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(request, request.POST)
        if form.is_valid():
            login(request, form.get_user())
            return redirect('todos:todo_list_list')  # Redirect to a page after login
    else:
        form = AuthenticationForm()
    return render(request, 'todos/login.html', {'form': form})

@login_required
def your_logout_view(request):
    logout(request)
    return render(request, 'todos/logout.html')