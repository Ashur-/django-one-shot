from django.contrib import admin
from .models import TodoList, TodoItem


# Register your models here.


class TodoListAdmin(admin.ModelAdmin):
    list_display = ("id", "name")


class TodoItemAdmin(admin.ModelAdmin):
    list_display = ["task", "due_date", "is_completed", "todo_list"]

    def is_completed(self, obj):
        return obj.completed is not None

    def todo_list(self, obj):
        return obj.list.name


admin.site.register(TodoList, TodoListAdmin)
admin.site.register(TodoItem, TodoItemAdmin)
