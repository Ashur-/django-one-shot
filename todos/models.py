from django.db import models
from django.utils import timezone

# Create your models here.


class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name


class TodoItem(models.Model):
    task = models.CharField(max_length=200)
    due_date = models.DateTimeField(blank=True, null=True)
    completed = models.DateTimeField(null=True, blank=True)
    list = models.ForeignKey(
        TodoList, on_delete=models.CASCADE, related_name="items"
    )


def is_completed(self):
    return self.completed is not None


is_completed.boolean = True
