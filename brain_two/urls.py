from django.contrib import admin
from django.urls import include, path
from todos.views import your_signup_view
from django.contrib.auth import views as auth_views

urlpatterns = [
    path("todos/", include("todos.urls")),
    path("admin/", admin.site.urls),
    path('login/', auth_views.LoginView.as_view(template_name='registration/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('signup/', your_signup_view, name='signup'),
]
